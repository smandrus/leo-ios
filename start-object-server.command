#!/bin/bash -e

force_kill=false

while getopts ":fh" opt; do
  case $opt in
    f)
      force_kill=true
      ;;
    h)
      echo "usage: start-object-server.command [-f]"
      exit
      ;;
    \?)
      echo "start-object-server.command: invalid option -$OPTARG"
      echo "usage: start-object-server.command [-f]"
      exit
      ;;
  esac
done

possible_package="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

package="$possible_package"
for file in "$possible_package"/realm-object-server*; do
    if [ -d "$file" ]; then
        package="$file"
        break
    fi
done

source "$package/shell.command"

existing_object_process_count=$(ps ax | grep "$package/[n]ode_modules" | wc -l)
if [ "$existing_object_process_count" -gt 0 ]; then
    if "$force_kill"; then
        pkill -f "$package/node_modules"
    else
        read -p "Another instance of the Object Server is already running. Type 'yes' if you want to terminate it: " confirmation
        confirmation_lower=$(echo "$confirmation" | tr '[:upper:]' '[:lower:]')
        if [ "$confirmation_lower" = "yes" ]; then
            echo "Terminating the other server instance"
            pkill -f "$package/node_modules"
        else
            echo "You typed '$confirmation'. Two instances of the server cannot run at the same time. Exiting."
            exit
        fi
    fi
fi

if [ ! -d "$package/object-server/keys" ]; then
    echo "Creating new public and private keypair"
    mkdir "$package/object-server/keys"
    openssl genrsa -out "$package/object-server/keys/token-signature.key" 2048 &> /dev/null
    openssl rsa -in "$package/object-server/keys/token-signature.key" -outform PEM -pubout -out "$package/object-server/keys/token-signature.pub" &> /dev/null

    echo "Generating Realm Sync Admin Credentials"

    "$package/realm-generate-admin-token" \
        -k "$package/object-server/keys/token-signature.key" \
        -o "$package/admin_token.base64" \
        -v
fi

echo
echo "Your admin access token is: $(cat "$package/admin_token.base64")"

if [ ! -d "$package/object-server/root_dir" ]; then
    echo "Creating server root directory"
    mkdir "$package/object-server/root_dir"
fi

if [ ! -d "$package/object-server/temp_dir" ]; then
    echo "Creating server temp directory"
    mkdir "$package/object-server/temp_dir"
fi

if [ ! -f "$package/object-server/installed" ]; then
    echo "Deleting files from the previous version"
    rm -rf ~/Library/Containers/io.realm.{RealmTasks,realmbrowser,realmtasks.macos}
    rm -rf ~/Library/Application\ Support/io.realm.realmtasks.macos/
    touch "$package/object-server/installed"
fi

pushd "$package/object-server"
node "$package/node_modules/.bin/realm-object-server" -c configuration.yml &
server_pid=$!
popd
if [ ! -f "$package/object-server/do_not_open_browser" ]; then
    # sleep here to let server start before opening browser (avoid can't load bug #577)
    sleep 2
    open http://localhost:9080
fi
wait $server_pid
