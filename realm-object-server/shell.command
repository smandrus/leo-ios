#!/bin/bash

PACKAGE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PREFIX="$PACKAGE/.prefix"

export PATH="$PACKAGE/node_modules/.bin":"$PREFIX/bin":$PATH

export LIBRARY_PATH="$PREFIX/lib"
export CPATH="$PREFIX/include"

export OPENSSL_CONF="$PREFIX/ssl/openssl.cnf"
export NPM_CONFIG_PREFIX="$PREFIX"

if [[ "${BASH_SOURCE[0]}" = "${0}" ]]; then
    clear
    cd "$PACKAGE" && exec $SHELL "$@"
fi
