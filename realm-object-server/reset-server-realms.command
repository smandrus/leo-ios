#!/bin/bash -e

possible_package="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

package="$possible_package"
for file in "$possible_package"/realm-object-server-*; do
    if [ -d "$file" ]; then
        package="$file"
        break
    fi
done

reset_folder(){
    local folder_path=$1
    rm -rf "$folder_path"
    mkdir "$folder_path"
}

reset_realmtasks(){
    echo "Resetting RealmTasks for macOS"
    rm -rf "$HOME/Library/Containers/io.realm.realmtasks.macos"
    rm -rf "$HOME/Library/Application Support/io.realm.realmtasks.macos/"
}

read -p "This will delete all the realms on the server and reset RealmTasks for macOS. Type 'yes' to confirm deletion of all realms: " confirmation
confirmation_lower=$(echo "$confirmation" | tr '[:upper:]' '[:lower:]')

if [ "$confirmation_lower" = "yes" ]; then
    echo "Resetting the state of the server."
    reset_folder "$package/object-server/root_dir/"
    reset_folder "$package/object-server/temp_dir/"
    reset_folder "$package/object-server/realm-object-server/"
    reset_realmtasks

    echo "The state of the server was reset. Please make sure that the client realms are reset as well."
else
    echo "You typed '$confirmation'. Cancelling."
fi
