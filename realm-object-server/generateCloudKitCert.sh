#!/bin/sh

openssl ecparam -name prime256v1 -genkey -noout -out eckey.pem
openssl ec -in eckey.pem -pubout
