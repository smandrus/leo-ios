//
//  RSLocationManager.swift
//  LeoRestaurant
//
//  Created by Hyung Jip Moon on 2017-05-07.
//  Copyright © 2017 leomoon. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import RealmSwift


enum LocationManagerStatus{
    case notauthorized  // the user hasn't allowed us to access their location
    case uninitialized  // user has authed, but locationmanager has yet to return 1st position
    case running        // we're running, will update location as necessary
    case paused         // for some reason cllocation has paused locaton updates (usally poor GPS signal)
    case stopped        // cllocation manager updates have been stopped
}

@objc class RSLocationManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = RSLocationManager()
    
    var locationManager: CLLocationManager?
    var currentState:LocationManagerStatus = .uninitialized
    var lastLocation: CLLocationCoordinate2D?
    var lastLocationName = ""       // every time we get a coordinate, reverse it to a human readable name
    var lastUpdatedAt: Date?
    var continuousUpdateMode = true
    
    var identity: String?
    
    var tokensToSkipOnUpdate = [NotificationToken]()
    
    
    override init() {
        super.init()
        
        DispatchQueue.main.async {
            self.locationManager = CLLocationManager()
            self.locationManager!.delegate = self
            self.start(realmIdentity: nil)
        }
    }
    
    func start(realmIdentity: String?)
    {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        if realmIdentity != nil && self.identity == nil {
            self.identity = realmIdentity!
        }

        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager!.startUpdatingLocation()
        }
        self.currentState = .running
    }
    
    func stop() {

        currentState = .stopped
    }
    
    
    func lastKnownLocation() -> (lastLocation: CLLocationCoordinate2D?, near: String?, at: Date? ) {
        // this is the default in case CLLocationManager isn't running or not allowed
        if currentState == .uninitialized || currentState == .notauthorized  {
            return (nil, nil, nil)
        }
        return (lastLocation, lastLocationName, lastUpdatedAt)
    }
    
    func state() -> LocationManagerStatus {
        return self.currentState
    }
    
    
    internal func updatePresenceForIdentity(identity: String?) {
        guard identity != nil else {
            return
        }
        let rlm = try! Realm()
        let myUserRecord = rlm.objects(RSUser.self).filter("id = %@", identity!).first
        myUserRecord?.updatePresence(tokensToSkipOnUpdate: tokensToSkipOnUpdate)
    }
    
    
    
    // MARK: CLLocationManagerDelegate Delegates
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lastLocation = locations.last?.coordinate
        
        lastUpdatedAt = Date()
        
        if self.identity != nil {
            self.updatePresenceForIdentity(identity: self.identity)
        }

    }
    
    internal func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        lastLocation!.latitude = (manager.location?.coordinate.latitude)!
        lastLocation!.longitude = (manager.location?.coordinate.longitude)!
        currentState = .running
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error")
    }
    
    
    internal func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        currentState = .paused
        print("location updates paused at \(NSDate())")
    }
    
    @nonobjc internal func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            currentState = .notauthorized
            print ("didChangeAuthorizationStatus to \(status)")
        case .restricted:
            print ("didChangeAuthorizationStatus to \(status)")
        case .denied:
            print ("didChangeAuthorizationStatus to \(status)")
            currentState = .notauthorized
            stop()
        case .authorizedAlways,
             .authorizedWhenInUse:
            print ("didChangeAuthorizationStatus to \(status)")
            currentState = .running
            start(realmIdentity: nil)
        }
    }
}
