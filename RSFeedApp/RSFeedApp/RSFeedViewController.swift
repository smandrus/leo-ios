//
//  RSFeedViewController.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-09.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit

class RSFeedViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var googleMapView: GMSMapView!
   // @IBOutlet weak var pinIconImageView: UIImageView!
    
    var searchedTypes = ["restaurant"]
    let locationManager = CLLocationManager()
    let dataProvider = RSNetworkManager()
    let searchRadius: Double = 40000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.startUpdatingLocation()
        googleMapView.isMyLocationEnabled = true
        googleMapView.settings.myLocationButton = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        self.googleMapView.delegate = self
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

//        if(segue.identifier == "showDetailVC") {
//            /* */
//            //let marker : GMSMarker
//            
//            var pinInfo = [RSPin]()
//            
//
//            let myVC = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! RSDetailViewController
//           // myVC.dataPassed = pinInfo
//            //navigationController?.pushViewController(myVC, animated: true)
//            
//            /**/
//            
//        }
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            googleMapView.isMyLocationEnabled = true
            googleMapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            googleMapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
            fetchNearbyPlaces(location.coordinate)
        }
    }
    
    func fetchNearbyPlaces(_ coordinate: CLLocationCoordinate2D) {
        googleMapView.clear()
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
            for place: RSFeedItem in places {
                let marker = RSPin(place: place)
                marker.map = self.googleMapView
            }
        }
    }
}

extension UIView {
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
}

// MARK: - GMSMapViewDelegate
extension RSFeedViewController {
    
    func googleMapView(_ googleMapView: GMSMapView!, willMove gesture: Bool) {
        
        if (gesture) {
            googleMapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        let pinInfo = marker as! RSPin
        let player1 = RSName()
        if let infoView = UIView.viewFromNibName("RSFeedPinView") as? RSFeedPinView {
            infoView.feedNameLabel.text = pinInfo.place.name
            
            if let photo = pinInfo.place.photo {
                infoView.feedImageView.image = photo
            } else {
                infoView.feedImageView.image = UIImage(named: "generic")
            }
            /* */
            
            let myVC = storyboard?.instantiateViewController(withIdentifier: "detailVC") as! RSDetailViewController
            myVC.placeNamePassedData = pinInfo.place.name
            myVC.latitudePassedData = pinInfo.place.coordinate.latitude
            myVC.longtitudePassedData = pinInfo.place.coordinate.longitude
            myVC.addressPassedData = pinInfo.place.address
            myVC.picturePassedData = pinInfo.place.photo
            player1.name = pinInfo.place.name
            myVC.receivedPlayer1 = player1
            
            navigationController?.pushViewController(myVC, animated: true)
            /**/
        }
    }

    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        let pinInfo = marker as! RSPin
        
        if let infoView = UIView.viewFromNibName("RSFeedPinView") as? RSFeedPinView {
            infoView.feedNameLabel.text = pinInfo.place.name
            
            if let photo = pinInfo.place.photo {
                infoView.feedImageView.image = photo
            } else {
                infoView.feedImageView.image = UIImage(named: "generic")
            }
            return infoView
        }
        else {
            return nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        googleMapView.selectedMarker = nil
        return false
    }
    func mapView(mapView: GMSMapView!, didChangeCameraPosition position: GMSCameraPosition!) {
        fetchNearbyPlaces(position.target)
    }
}
