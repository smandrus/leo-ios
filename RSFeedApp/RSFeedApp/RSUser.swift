
import Foundation
import CoreLocation

import RealmSwift

private var realm: Realm!


// MARK: User
class RSUser : Object {

    dynamic var id = ""
  
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["role"]
    }
    
    convenience init(realmIdentity: String?) {
        self.init()
        self.id = realmIdentity!
    }
    
    convenience init(realmIdentity: String?, firstName: String?, lastName: String?) {
        self.init()
        self.id = realmIdentity!

    }
    
    func updatePresence(tokensToSkipOnUpdate: [NotificationToken]) {
        
        let realm = try! Realm()
        
        try! realm.write {

            // Write user's favorite restaurant to Realm
        }
    }
}
