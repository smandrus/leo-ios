//
//  RSDetailViewController.swift
//  RSFeedApp
//
//  Created by Hyung Jip Moon on 2017-05-10.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

var restaurantNameArray = [String]()

class RSDetailViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate  {
    
    var receivedPlayer1 = RSName()

    let realm = try! Realm()

    
    @IBOutlet weak var detailGoogleMapView: GMSMapView!
    
    @IBOutlet weak var removeFromFavorites: UIButton!
    @IBOutlet weak var addToFavoritesButton: UIButton!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    @IBOutlet weak var restaurantAddressLabel: UILabel!
    
    @IBOutlet weak var restaurantImageView: UIImageView!
    
    let locationManager = CLLocationManager()

    var placeNamePassedData: String!
    var latitudePassedData: Double!
    var longtitudePassedData: Double!
    var addressPassedData: String!
    var picturePassedData: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formDetailView()
    }

    

    func formDetailView() {
        locationManager.startUpdatingLocation()
        detailGoogleMapView.isMyLocationEnabled = true
        detailGoogleMapView.settings.myLocationButton = true
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        self.detailGoogleMapView.delegate = self
        
        let position = CLLocationCoordinate2D(latitude: latitudePassedData, longitude: longtitudePassedData)
        let marker = GMSMarker(position: position)
        marker.title = "Hello World"
        marker.map = detailGoogleMapView
        
        detailGoogleMapView.camera = GMSCameraPosition.camera(withLatitude: latitudePassedData,
                                                              longitude: longtitudePassedData,
                                                              zoom: 16)
        locationManager.stopUpdatingLocation()
        
        restaurantNameLabel.text = placeNamePassedData
        restaurantAddressLabel.text = addressPassedData
        restaurantImageView.image = picturePassedData
    }
    @IBAction func addToFavoritesButtonTapped(_ sender: Any) {
        
        self.testFunction(user: receivedPlayer1)
    }
    
    func testFunction(user: RSName) {
        
        let rlm = try! Realm()
        let identity = user.name
        var userRestaurantRecord = rlm.objects(RSName.self).filter(NSPredicate(format: "name = %@", identity)).first
//        print(Object.primaryKey() as Any)
//        print(userRestaurantRecord as Any)
        
        try! rlm.write {
            if userRestaurantRecord == nil {

                
                print("\n\nCreating new user restaurnt record for user id: \(identity)\n")
                userRestaurantRecord = rlm.create(RSName.self, value: ["name": identity, "creationDate": Date()])
                rlm.add(userRestaurantRecord!, update: true)
                
                restaurantNameArray.append(identity)
                let objects = List<RSName>()
                objects.append(RSName())
            } else {
                print("Found user restaurant record, details: \(String(describing: userRestaurantRecord))\n")
            }
        }

    }
    
    @IBAction func removeFromFavoritesButtonTapped(_ sender: Any) {
        
    }
    
}
