//
//  RSLoginViewController.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-08.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit
import QuartzCore

import Realm
import RealmSwift
import FacebookLogin
import FBSDKLoginKit
import FacebookCore

class RSLoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    let loginToTabViewSegue         = "loginToTabViewSegue"
    var token: NotificationToken!
    
    @IBOutlet weak var emailLoginTextField: UITextField!
    @IBOutlet weak var passwordLoginTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var goToSignUpButton: UIButton!
    
    @IBOutlet weak var facebokLoginButton: FBSDKLoginButton!

    func infoServer() -> (authScheme: String, formattedURL: String) {
        var authScheme = "http"
        var scheme: String?
        var formattedURL = RSConstants.serverURL
        if let schemeRange = formattedURL?.range(of: "://") {
            scheme = formattedURL?.substring(to: schemeRange.lowerBound)
            if scheme == "realms" || scheme == "https" {
                RSConstants.serverPort = 9443
                authScheme = "https"
            }
            formattedURL = formattedURL?.substring(from: schemeRange.upperBound)
        }
        if let portRange = formattedURL?.range(of: ":") {
            if let portString = formattedURL?.substring(from: portRange.upperBound) {
                RSConstants.serverPort = Int(portString) ?? RSConstants.serverPort
            }
            formattedURL = formattedURL?.substring(to: portRange.lowerBound)
        }
        return (authScheme, formattedURL!)
    }

    func submitLogin(username: String?, password: String?) {
        
        saveLoginCredentials()
        
        let credentials = RLMSyncCredentials(username: username!, password: password!, register: false)
        RLMSyncUser.__logIn(with: credentials, authServerURL: URL(string: "\(infoServer().authScheme)://\(infoServer().formattedURL):\(RSConstants.serverPort)")!, timeout: 30, onCompletion: { (user, error) in
            DispatchQueue.main.async {
                
                if let error = error {
                    let alertController = UIAlertController(title: "Unable to Sign In", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                RSConstants.loginSuccessfulHandler?(user!)
            }
        })
        

    }
    
    private func saveLoginCredentials() {
        let userDefaults = UserDefaults.standard
    
        userDefaults.set(nil, forKey: RSConstants.serverURLKey)
        userDefaults.set(nil, forKey: RSConstants.emailKey)
        userDefaults.set(nil, forKey: RSConstants.passwordKey)
        
        
        userDefaults.synchronize()
    }
    
    private func loadLoginCredentials() {
        let userDefaults = UserDefaults.standard
        RSConstants.serverURL = userDefaults.string(forKey: RSConstants.serverURLKey)
        RSConstants.username = userDefaults.string(forKey:  RSConstants.emailKey)
        RSConstants.password = userDefaults.string(forKey: RSConstants.passwordKey)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupErrorHandler()
        
        facebookLoginHelper()
    }
    
    func facebookLoginHelper() {
        if (FBSDKAccessToken.current() != nil) {
            print("User logged in...")
        } else {
            print("User not logged in...")
        }
        let fbLoginButton: FBSDKLoginButton! = FBSDKLoginButton()
        self.view.addSubview(fbLoginButton)
        fbLoginButton.delegate = self
        fbLoginButton.center = self.view.center
        fbLoginButton.readPermissions = ["public_profile", "email", "user_friends"]

    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            let facebookCredentials = RLMSyncCredentials.init(facebookToken: FBSDKAccessToken.current().tokenString)
            RLMSyncUser.__logIn(with: facebookCredentials, authServerURL: URL(string: "\(infoServer().authScheme)://\(infoServer().formattedURL):\(RSConstants.serverPort)")!, timeout: 30, onCompletion: { (user, error) in
                DispatchQueue.main.async {
                    
                    if let error = error {
                        let alertController = UIAlertController(title: "Unable to Sign In", message: error.localizedDescription, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                        
                        return
                    }
                    
                    RSConstants.loginSuccessfulHandler?(user!)
                }
            })
            
//            if result.grantedPermissions.contains("email")
//            {
//                // Do work
//            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (SyncUser.current != nil) {
            setDefaultRealmConfigurationWithUser(user: SyncUser.current!)
            performSegue(withIdentifier: loginToTabViewSegue, sender: self)
            
        } else {
            
            RSConstants.serverURL = RSConstants.syncHost
            
            RSConstants.loginSuccessfulHandler = { user in
                DispatchQueue.main.async {
                    self.completeLogin(user: user) //  connects the realm and looks up or creates a profile
                    //loginViewController.dismiss(animated: true, completion: nil)
                    self.performSegue(withIdentifier: self.loginToTabViewSegue, sender: nil)
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == loginToTabViewSegue {
            
        }
    }
    
    func completeLogin(user: SyncUser?) {
        setDefaultRealmConfigurationWithUser(user: user!)
        
        let rlm = try! Realm()
        let identity = (user!.identity)!
        var myUserRecord = rlm.objects(RSUser.self).filter(NSPredicate(format: "id = %@", identity)).first
        
        try! rlm.write {
            if myUserRecord == nil {
                print("\n\nCreating new user record for user id: \(identity)\n")
                myUserRecord = rlm.create(RSUser.self, value: ["id": identity, "creationDate": Date()])
                rlm.add(myUserRecord!, update: true)
            } else {
                print("Found user record, details: \(String(describing: myUserRecord))\n")
            }
        }
        
        setupDefaultGlobalPermissions(user: user)
        
        if RSLocationManager.sharedInstance.currentState != .running {
            let locationShim = RSLocationManager.sharedInstance
            if locationShim.identity == nil {
                locationShim.identity = identity
            }
        }
    }
    
    func setupDefaultGlobalPermissions(user: SyncUser?) {
        
        let managementRealm = try! user!.managementRealm()
        let theURL = RSConstants.commonRealmURL.absoluteString
        
        let permissionChange = SyncPermissionChange(realmURL: theURL,    // The remote Realm URL on which to apply the changes
            userID: "*",       // The user ID for which these permission changes should be applied
            mayRead: true,     // Grant read access
            mayWrite: true,    // Grant write access
            mayManage: false)  // Grant management access
        
        token = managementRealm.objects(SyncPermissionChange.self).filter("id = %@", permissionChange.id).addNotificationBlock { notification in
            if case .update(let changes, _, _, _) = notification, let change = changes.first {
                // Object Server processed the permission change operation
                switch change.status {
                case .notProcessed:
                    print("not processed.")
                case .success:
                    print("succeeded.")
                case .error:
                    print("Error.")
                }
                print("change notification: \(change.debugDescription)")
            }
        }
        
        try! managementRealm.write {
            print("Launching permission change request id: \(permissionChange.id)")
            managementRealm.add(permissionChange)
        }
    }
    
    // MARK: Realm Connection Utils
    func configureDefaultRealm() -> Bool {
        if let user = SyncUser.current {
            setDefaultRealmConfigurationWithUser(user: user)
            return true
        }
        return false
    }
    
    
    func setDefaultRealmConfigurationWithUser(user: SyncUser) {
        Realm.Configuration.defaultConfiguration = RSConstants.commonRealmConfig
    }
    
    
    func setupErrorHandler(){
        SyncManager.shared.errorHandler = { error, session in
            let syncError = error as! SyncError
            switch syncError.code {
            case .clientResetError:
                if let (path, runClientResetNow) = syncError.clientResetInfo() {
                    print(path)
                    runClientResetNow()
                }
            default: break
                // Handle other errors
            }
        }
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        submitLogin(username: emailLoginTextField.text!, password: passwordLoginTextField.text!)
    }
    @IBAction func goToSignUpButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToSignUpVC", sender: self)

    }
    
}
