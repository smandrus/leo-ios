//
//  RSFeedItem.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-10.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import SwiftyJSON

class RSFeedItem {
    let name: String
    let address: String
    let coordinate: CLLocationCoordinate2D
    let placeType: String
    var photoReference: String?
    var photo: UIImage?
    
    init(dictionary:[String : AnyObject], acceptedTypes: [String])
    {
        let json = JSON(dictionary)
        name = json["name"].stringValue
        address = json["vicinity"].stringValue
        
        let lat = json["geometry"]["location"]["lat"].doubleValue as CLLocationDegrees
        let lng = json["geometry"]["location"]["lng"].doubleValue as CLLocationDegrees
        coordinate = CLLocationCoordinate2DMake(lat, lng)
        
        photoReference = json["photos"][0]["photo_reference"].string
        
        var foundType = "restaurant"
        let possibleTypes = acceptedTypes.count > 0 ? acceptedTypes : ["cafe", "restaurant"]
        for type in json["types"].arrayObject as! [String] {
            if possibleTypes.contains(type) {
                foundType = type
                break
            }
        }
        placeType = foundType
    }
}
