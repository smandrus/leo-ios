//
//  RSFeedPinView.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-10.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit

class RSFeedPinView: UIView {

    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var feedNameLabel: UILabel!

    /*
     @IBOutlet weak var feedImageView: UIImageView!
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
