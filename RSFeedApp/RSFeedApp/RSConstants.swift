//
//  LeoRestaurantConstants.swift
//  LeoRestaurant
//
//  Created by Hyung Jip Moon on 2017-05-07.
//  Copyright © 2017 leomoon. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import Realm
import RealmSwift

struct RSConstants {
    static let appID = Bundle.main.bundleIdentifier!
    
    static let syncHost                 = "localhost"
    static let ApplicationName          = "LeoRestaurant"
    static let syncRealmPath            = "leorestaurant"
    
    static let syncAuthURL = NSURL(string: "http://\(syncHost):9080")!
    
    static let syncServerURL = NSURL(string: "realm://\(syncHost):9080/\(ApplicationName)-\(syncRealmPath)")
    
    static let commonRealmURL = URL(string: "realm://\(syncHost):9080/\(ApplicationName)-CommonRealm")!
    static let commonRealmConfig = Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: commonRealmURL),
                                                       objectTypes: [RSUser.self, RSName.self])

    static var loginSuccessfulHandler: ((RLMSyncUser) -> Void)?
    
    /* Login/Register Credentials */
    static var serverURL: String?
    static var serverPort = 9080
    static var username: String?
    static var password: String?
    static var confirmPassword: String?
    
    /* User default keys for saving form data */
    static let serverURLKey = "RealmLoginServerURLKey"
    static let emailKey     = "RealmLoginEmailKey"
    static let passwordKey  = "RealmLoginPasswordKey"

}
