//
//  RSPin.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-10.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit

class RSPin: GMSMarker {
    let place: RSFeedItem
    
    init(place: RSFeedItem) {
        self.place = place
        super.init()
        
        position = place.coordinate
        groundAnchor = CGPoint(x: 0.5, y: 1)
    }
}
