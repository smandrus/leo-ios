//
//  RSName.swift
//  RealmPop
//
//  Created by Marin Todorov on 2/7/17.
//  Copyright © 2017 Realm Inc. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

private var realm: Realm!

class RSName: Object {

    dynamic var name = ""

    override static func primaryKey() -> String? {
        return "name"
    }

    convenience init(realmIdentity: String?) {
        self.init()
        self.name = realmIdentity!
    }

    convenience init(realmIdentity: String?, firstName: String?, lastName: String?) {
        self.init()
        self.name = realmIdentity!
    
    }

}
