//
//  RSMainScreenViewController.swift
//  LoginRegisterTest
//
//  Created by Hyung Jip Moon on 2017-05-09.
//  Copyright © 2017 leomoon. All rights reserved.
//

import UIKit
import RealmSwift

class RSMainScreenViewController: UITabBarController {
    override func viewDidLoad() {
        
        super.viewDidLoad()
        goToMainScreen()
        
        let btn1 = UIButton(type: .custom)
        
        btn1.setTitle("Sign Out", for: UIControlState.normal)
        btn1.setTitleColor(UIColor.init(colorLiteralRed: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0), for:UIControlState.normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 75, height: 30)
        btn1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        self.view.addSubview(btn1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    func buttonAction(sender: UIButton!) {
        
        let alert = UIAlertController(title: NSLocalizedString("Logout", comment: "Logout"), message: NSLocalizedString("Really Log Out?", comment: "Really Log Out?"), preferredStyle: .alert)
        
        // Logout button
        let OKAction = UIAlertAction(title: NSLocalizedString("Logout", comment: "logout"), style: .default) { (action:UIAlertAction!) in
            print("Logout button tapped");

            SyncUser.current?.logOut()
            //Now we need to segue to the login view controller
            self.performSegue(withIdentifier: "segueToLogin", sender: self)
        }
        alert.addAction(OKAction)
        
        // Cancel button
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alert.addAction(cancelAction)
        
        // Present Dialog message
        present(alert, animated: true, completion:nil)
    }
    
    func goToMainScreen() {
        self.presentedViewController?.dismiss(animated: true, completion: {
            
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    
    
}

